# A16_Team_17_Beer_Tag

Beer Tag web application.

Beer tag enables users to manage all the beers that they have drank and want to drink.Each beer has name, abv, style, brewery, country, one or more tags..
Users can create new beers, rate existing beers, add them to wish and drank list.
Users can be anonymous, regular or admin.
Anonymous users can browse all beers and their details. They can also filter by country, style and sort by name, ABV.

Registered users can create new beers, add a beer to his wish list and drunk list and rate a beer. They can also edit and delete their own beers. 

Administrators have full control over all beers and all users.

Data is stored in a MariaDB relational database. 
Project is tiered in 3 main layers - repository, service, and UI. 
Used SpringMVC and SpringBoot framework.
Used Hibernate/JPA in repository layer.
Used Spring Security to handle user registration and user roles.
Over 85% unit test coverage to the service layer.

For fronted is used mainly Bootstrap and some already done templates or pages from creative-tim.com.
Gitlab project link - https://gitlab.com/teamwork_team_17/a16_team_17_beer_tag
Trello link - https://trello.com/b/5jwpjrWa/teamworkbeertagwebapplication
Link to database scheme - https://my.vertabelo.com/public-model-view/G1DWLWzgpNbJ8MLISgLgsViuZTRO7aulfGagRqmSyegisMaCFIHzd5Jcvqh6USfO?x=4600&y=4808&zoom=0.7098