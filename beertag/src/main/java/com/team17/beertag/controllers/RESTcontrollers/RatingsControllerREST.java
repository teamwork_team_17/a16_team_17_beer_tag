package com.team17.beertag.controllers.RESTcontrollers;

import com.team17.beertag.exceptions.DuplicateEntityException;
import com.team17.beertag.exceptions.EntityNotFoundException;
import com.team17.beertag.models.Rating;
import com.team17.beertag.repositories.RatingsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/ratings")
public class RatingsControllerREST {
    private RatingsRepository ratingsRepository;

    @Autowired
    public RatingsControllerREST(RatingsRepository ratingsRepository) {
        this.ratingsRepository = ratingsRepository;
    }

    @PostMapping
    public void create(@RequestBody @Valid Rating rating) {
        try {
             ratingsRepository.create(rating);
        } catch (DuplicateEntityException dee) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, dee.getMessage());
        } catch (EntityNotFoundException nfe) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, nfe.getMessage());
        }
    }

    @GetMapping("/{id}")
    public double getRatingById(@PathVariable int id){
        try {
            return ratingsRepository.getAverageRatingForBeer(id);
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
    }

}
