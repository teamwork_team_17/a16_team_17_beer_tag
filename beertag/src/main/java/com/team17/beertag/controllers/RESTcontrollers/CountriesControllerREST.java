package com.team17.beertag.controllers.RESTcontrollers;

import com.team17.beertag.exceptions.DuplicateEntityException;
import com.team17.beertag.exceptions.EntityNotFoundException;
import com.team17.beertag.models.Country;
import com.team17.beertag.services.CountriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/countries")
public class CountriesControllerREST {
    private CountriesService countriesService;

    @Autowired
    public CountriesControllerREST(CountriesService countriesService) {
        this.countriesService = countriesService;
    }

    @GetMapping
    public List<Country> getAll(){
        return countriesService.getAll();
    }

    @GetMapping("/{id}")
    public Country getById(@PathVariable int id){
        try {
            return countriesService.getById(id);
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
    }

    @PostMapping
    public Country create(@RequestBody @Valid Country country){
        try {
            Country newCountry = new Country();
            newCountry.setName(country.getName());
            return countriesService.create(newCountry);
        }
        catch (DuplicateEntityException dee){
            throw new ResponseStatusException(HttpStatus.CONFLICT, dee.getMessage());
        }
        catch (EntityNotFoundException nfe){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,nfe.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Country update(@PathVariable int id, @RequestBody @Valid Country country){
        try {
            Country countryToUpdate = new Country();
            countryToUpdate.setId(id);
            countryToUpdate.setName(country.getName());
            countriesService.update(countryToUpdate);
            return countryToUpdate;
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id){
        try {
            countriesService.delete(id);
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
    }
}
