package com.team17.beertag.controllers.RESTcontrollers;

import com.team17.beertag.exceptions.DuplicateEntityException;
import com.team17.beertag.exceptions.EntityNotFoundException;
import com.team17.beertag.models.Beer;
import com.team17.beertag.models.Style;
import com.team17.beertag.services.StylesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/styles")
public class StylesControllerREST {
    private StylesService stylesService;

    @Autowired
    public StylesControllerREST(StylesService stylesService) {
        this.stylesService = stylesService;
    }

    @GetMapping
    public List<Style> getAll(){
        return stylesService.getAll();
    }

    @GetMapping("/{id}")
    public Style getById(@PathVariable int id){
        try {
            return stylesService.getById(id);
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
    }

    @GetMapping("/{styleId}/beers")
    public List<Beer> getStyleBeers(@PathVariable int styleId){
        try{
            return stylesService.getStyleBeers(styleId);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Style create(@RequestBody @Valid Style style){
        try {
            Style newStyle = new Style();
            newStyle.setName(style.getName());
            return stylesService.create(newStyle);
        }
        catch (DuplicateEntityException dee){
            throw new ResponseStatusException(HttpStatus.CONFLICT, dee.getMessage());
        }
        catch (EntityNotFoundException nfe){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,nfe.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Style update(@PathVariable int id, @RequestBody @Valid Style style){
        try {
            Style styleToUpdate = new Style();
            styleToUpdate.setId(id);
            styleToUpdate.setName(style.getName());
            stylesService.update(styleToUpdate);
            return styleToUpdate;
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id){
        try {
            stylesService.delete(id);
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
    }
}
