package com.team17.beertag.controllers.RESTcontrollers;

import com.team17.beertag.exceptions.DuplicateEntityException;
import com.team17.beertag.exceptions.EntityNotFoundException;
import com.team17.beertag.models.Tag;
import com.team17.beertag.services.TagsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/tags")
public class TagsControllerREST {
    private TagsService tagsService;

    @Autowired
    public TagsControllerREST(TagsService tagsService) {
        this.tagsService = tagsService;
    }

    @GetMapping
    public List<Tag> getAll() {
        return tagsService.getAll();
    }

    @GetMapping("/{id}")
    public Tag getById(@PathVariable int id) {
        try {
            return tagsService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Tag create(@RequestBody @Valid Tag tag) {
        try {
            Tag newTag = new Tag();
            newTag.setName(tag.getName());
            return tagsService.create(newTag);
        } catch (DuplicateEntityException dee) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, dee.getMessage());
        } catch (EntityNotFoundException nfe) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, nfe.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Tag update(@PathVariable int id, @RequestBody @Valid Tag tag) {
        try {
            Tag tagToUpdate = new Tag();
            tagToUpdate.setId(id);
            tagToUpdate.setName(tag.getName());
            tagsService.update(tagToUpdate);
            return tagToUpdate;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            tagsService.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
