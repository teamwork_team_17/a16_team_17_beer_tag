package com.team17.beertag.controllers;

import com.team17.beertag.models.*;
import com.team17.beertag.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
public class BeersController {
    private BeersService service;
    private StylesService stylesService;
    private CountriesService countriesService;
    private BreweriesService breweriesService;
    private UsersService usersService;
    private RatingsService ratingsService;
    private TagsService tagsService;

    @Autowired
    public BeersController(BeersService service,
                           StylesService stylesService,
                           CountriesService countriesService,
                           BreweriesService breweriesService,
                           UsersService usersService,
                           RatingsService ratingsService,
                           TagsService tagsService) {
        this.service = service;
        this.stylesService = stylesService;
        this.countriesService = countriesService;
        this.breweriesService = breweriesService;
        this.usersService = usersService;
        this.ratingsService = ratingsService;
        this.tagsService = tagsService;
    }

    @GetMapping("/beers")
    public String showBeers(@RequestParam(value = "country", required = false) Integer country,
                            @RequestParam(value = "style", required = false) Integer style,
                            @RequestParam(value = "sortBy", required = false) String sortBy,
                            Model model) {

        if (country != null || style != null || sortBy != null) {
            model.addAttribute("beers", service.getFilteredAndSorted(country, style, sortBy));
        } else {
            model.addAttribute("beers", service.getAll());
        }
        return "beers";
    }

    @GetMapping("/beers/new")
    public String showNewBeerForm(Model model) {
        model.addAttribute("beer", new BeerDto());
        return "beer";
    }

    @PostMapping("/beers/new")
    public String createBeer(@Valid @ModelAttribute("beer") BeerDto beer,
                             BindingResult errors,
                             @RequestParam("file") MultipartFile file) {
        if (errors.hasErrors()) {
            return "beer";
        }
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Beer toCreate = new Beer();
        toCreate.setName(beer.getName());
        toCreate.setAbv(beer.getAbv());
        toCreate.setStyle(stylesService.getById(beer.getStyleId()));
        toCreate.setBrewery(breweriesService.getById(beer.getBreweryId()));
        toCreate.setCountry(countriesService.getById(beer.getCountryId()));
        toCreate.setCreatedBy(usersService.getByUsername(authentication.getName()).get(0));

        try {
            if(!file.isEmpty()) {
                toCreate.setImage(file.getBytes());
            }
        } catch (IOException e) {

        }
        service.create(toCreate);
        return "redirect:/beers";
    }

    @GetMapping("/beers/{id}")
    public String showBeerForm(Model model, @PathVariable String id) {
        model.addAttribute("beer", service.getById(Integer.parseInt(id)));
        return "beerPage";
    }

    @GetMapping("/beers/{id}/edit")
    public String showEditBeerForm(Model model, @PathVariable int id) {
        Beer beer = service.getById(id);
        BeerDto beerDto = new BeerDto();
        beerDto.setName(beer.getName());
        beerDto.setAbv(beer.getAbv());
        beerDto.setStyleId(beer.getStyle().getId());
        beerDto.setBreweryId(beer.getBrewery().getId());
        beerDto.setCountryId(beer.getCountry().getId());
        beerDto.setImage(beer.getImage());
        beerDto.setDescription(beer.getDescription());
        model.addAttribute("beer", beerDto);
        return "editBeer";
    }

    @PostMapping("/beers/{id}/edit")
    public String editBeer(@Valid @ModelAttribute("beer") BeerDto beer,
                           BindingResult errors,
                           @RequestParam("file") MultipartFile file,
                           @PathVariable("id") int id) {
        if (errors.hasErrors()) {
            return "beerPage";
        }
        Beer toUpdate = service.getById(id);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo userToEdit = usersService.getByUsername(authentication.getName()).get(0);

        toUpdate.setName(beer.getName());
        toUpdate.setAbv(beer.getAbv());
        toUpdate.setStyle(stylesService.getById(beer.getStyleId()));
        toUpdate.setBrewery(breweriesService.getById(beer.getBreweryId()));
        toUpdate.setCountry(countriesService.getById(beer.getCountryId()));
        toUpdate.setDescription(beer.getDescription());
        try {
            if (!file.isEmpty()) {
                toUpdate.setImage(file.getBytes());
            }
        } catch (IOException e) {

        }
        service.update(toUpdate, userToEdit);
        return "redirect:/beers";
    }


    @PostMapping("/beers/{id}/delete")
    public String deleteBeer(Model model, @PathVariable int id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo userToEdit = usersService.getByUsername(authentication.getName()).get(0);
        Beer beer = service.getById(id);

        if (userToEdit.getWishList().stream().filter(beer1 -> beer.getId() == id).count() == 1) {
            service.deleteBeerFromWishList(id, userToEdit.getId());
        }

        if (userToEdit.getDrankList().stream().filter(beer1 -> beer.getId() == id).count() == 1) {
            service.deleteBeerFromDrankList(id, userToEdit.getId());
        }
        service.delete(id, userToEdit);
        return "redirect:/beers";
    }

    @PostMapping("/beers/{id}/wishlist")
    public String addBeerToWishList(Model model, @PathVariable int id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo user = usersService.getByUsername(authentication.getName()).get(0);
        Beer beer = service.getById(id);
        Set<Beer> wishList = usersService.getWishList(user.getId());

        if (wishList.stream().noneMatch(beer1 -> beer1.getName().equals(beer.getName()))) {
            service.addBeerToWishList(id, user.getId());
        }

        return "redirect:/beers/" + id;
    }

    @PostMapping("/beers/{id}/rate")
    public String rateBeer(@PathVariable int id, @RequestParam(value = "rating") int rating) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = usersService.getByUsername(authentication.getName()).get(0);
        Beer beer = service.getById(id);

        Rating ratingToAdd = new Rating();
        ratingToAdd.setBeerId(beer);
        ratingToAdd.setUserInfoId(userInfo);
        ratingToAdd.setRating(Double.parseDouble(Integer.toString(rating)));
        if (beer.getRatings().stream().filter(rating1 -> rating1.getUserInfoId().getId() == userInfo.getId()).count() == 0) {
            ratingsService.create(ratingToAdd);
        }

        return "redirect:/beers/" + id;
    }

    @PostMapping("/beers/{id}/dranklist")
    public String addBeerToDrankList(Model model, @PathVariable int id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo user = usersService.getByUsername(authentication.getName()).get(0);

        service.deleteBeerFromWishList(id, user.getId());

        service.addBeerToDrankList(id, user.getId());
        return "redirect:/beers/" + id;
    }

    @ModelAttribute("styles")
    public List<Style> populateStyles() {
        return stylesService.getAll();
    }

    @ModelAttribute("countries")
    public List<Country> populateCountries() {
        return countriesService.getAll();
    }

    @ModelAttribute("breweries")
    public List<Brewery> populateBreweries() {
        return breweriesService.getAll();
    }

    @ModelAttribute("tags")
    public List<Tag> populateTags() {
        return tagsService.getAll();
    }

}
