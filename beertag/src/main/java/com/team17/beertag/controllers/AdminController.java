package com.team17.beertag.controllers;

import com.team17.beertag.models.Beer;
import com.team17.beertag.models.BeerDto;
import com.team17.beertag.models.UserInfo;
import com.team17.beertag.services.BeersService;
import com.team17.beertag.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Set;

@Controller
public class AdminController {
    private UsersService service;
    private UserDetailsManager userDetailsManager;
    private BeersService beersService;

    @Autowired
    public AdminController(UsersService service, UserDetailsManager userDetailsManager, BeersService beersService) {
        this.service = service;
        this.userDetailsManager = userDetailsManager;
        this.beersService = beersService;
    }

    @GetMapping("/admin")
    public String adminPage(Model model) {
        model.addAttribute("users", service.getAll());
        return "admin";
    }

    @PostMapping("/admin/{id}/delete")
    public String deleteUser(@PathVariable int id) {
        UserInfo userInfo = service.getById(id);
        List<Beer> beerList = beersService.createdBy(userInfo);
        for (Beer beer : beerList) {
            int beerId = beer.getId();

            deleteFromDrankList(beerId);
            deleteFromWishList(beerId);

            beersService.delete(beerId, userInfo);
        }
        service.delete(id);
        userDetailsManager.deleteUser(userInfo.getUsername());

        return "redirect:/admin";
    }

    @GetMapping("/admin/{id}/edit")
    public String showEditUserForm(@PathVariable int id, Model model) {
        UserInfo userInfo = service.getById(id);
        model.addAttribute("user", userInfo);
        return "editUser";
    }

    @PostMapping("/admin/{id}/edit")
    public String editUser(@Valid @ModelAttribute("user") UserInfo userInfo,
                           BindingResult errors,
                           @RequestParam("file") MultipartFile file,
                           @PathVariable("id") int id) {
        if (errors.hasErrors()) {
            return "admin";
        }

        UserInfo userToEdit = service.getById(id);
        userToEdit.setFirstName(userInfo.getFirstName());
        userToEdit.setLastName(userInfo.getLastName());
        userToEdit.setEmail(userInfo.getEmail());
        try {
            if (!file.isEmpty()) {
                userToEdit.setImage(file.getBytes());
            }
        } catch (IOException e) {

        }
        service.update(userToEdit);

        return "redirect:/admin";
    }

    private void deleteFromWishList(int beerId) {
        Set<UserInfo> userListWish = service.getAllUsersBeerInWishList(beerId);
        for (UserInfo user : userListWish) {
            int userId = user.getId();
            beersService.deleteBeerFromWishList(beerId, userId);
        }
    }

    private void deleteFromDrankList(int beerId) {
        Set<UserInfo> userListDrank = service.getAllUsersBeerInDrankList(beerId);
        for (UserInfo user : userListDrank) {
            int userId = user.getId();
            beersService.deleteBeerFromDrankList(beerId, userId);
        }
    }
}
