package com.team17.beertag.controllers.RESTcontrollers;

import com.team17.beertag.exceptions.DuplicateEntityException;
import com.team17.beertag.exceptions.EntityNotFoundException;
import com.team17.beertag.exceptions.InvalidOperationException;
import com.team17.beertag.models.*;
import com.team17.beertag.services.BeersService;
import com.team17.beertag.services.StylesService;
import com.team17.beertag.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/beers")
public class BeersControllerREST {
    private BeersService service;
    private UsersService usersService;
    private StylesService stylesService;
    private DtoMapper mapper;

    @Autowired
    public BeersControllerREST(BeersService service, DtoMapper mapper, StylesService stylesService, UsersService usersService) {
        this.service = service;
        this.mapper = mapper;
        this.stylesService = stylesService;
        this.usersService = usersService;
    }

    @GetMapping
    public List<Beer> getAll(@RequestParam(required = false) String name,
                             @RequestParam(required = false) Integer style,
                             @RequestParam(required = false) Integer country,
                             @RequestParam(required = false) String sort){
        if(name!=null&&!name.isEmpty()){
            return service.getByName(name);
        }

        if(style!=null){
            return service.getByStyle(style);
        }

        if(country!=null){
            return service.getByCountry(country);
        }

        if(sort!=null&&!sort.isEmpty()){
            return service.sortBy(sort);
        }

        return service.getAll();
    }

    @GetMapping("/{id}")
    public Beer getById(@PathVariable int id){
        try {
            return service.getById(id);
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
    }

    @PostMapping
    public Beer create(@RequestBody @Valid BeerDto beerDto, @RequestHeader(name = "Authorization") int authorization){
        try {
            UserInfo requestUserInfo = usersService.getById(authorization);
            Beer newBeer = mapper.fromDto(beerDto);
            newBeer.setCreatedBy(requestUserInfo);
            return service.create(newBeer);
        }
        catch (DuplicateEntityException dee){
            throw new ResponseStatusException(HttpStatus.CONFLICT, dee.getMessage());
        }
        catch (EntityNotFoundException nfe){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,nfe.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Beer update(@PathVariable int id,
                       @RequestBody @Valid BeerDto beerDto,
                       @RequestHeader(name = "Authorization") int authorization){
        try {
            UserInfo requestUserInfo = usersService.getById(authorization);

            Beer beer = service.getById(id);
            beer.setName(beerDto.getName());
            beer.setAbv(beerDto.getAbv());
            beer.setStyle(stylesService.getById(beerDto.getStyleId()));
            beer.setId(id);
            service.update(beer, requestUserInfo);
            return beer;
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        } catch (InvalidOperationException ioe){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ioe.getLocalizedMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id,
                       @RequestHeader(name = "Authorization") int authorization){
        try {
            UserInfo requestUserInfo = usersService.getById(authorization);

            service.delete(id, requestUserInfo);
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        } catch (InvalidOperationException ioe){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ioe.getLocalizedMessage());
        }
    }
}

