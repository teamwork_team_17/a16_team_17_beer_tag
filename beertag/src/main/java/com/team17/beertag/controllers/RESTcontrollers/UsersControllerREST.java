package com.team17.beertag.controllers.RESTcontrollers;

import com.team17.beertag.exceptions.DuplicateEntityException;
import com.team17.beertag.exceptions.EntityNotFoundException;
import com.team17.beertag.models.Beer;
import com.team17.beertag.models.UserInfo;
import com.team17.beertag.models.UserDto;
import com.team17.beertag.models.UserDtoMapper;
import com.team17.beertag.services.BeersService;
import com.team17.beertag.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/users")
public class UsersControllerREST {
    UsersService service;
    BeersService beersService;
    UserDtoMapper mapper;

    @Autowired
    public UsersControllerREST(UsersService service, UserDtoMapper mapper, BeersService beersService) {
        this.service = service;
        this.mapper = mapper;
        this.beersService = beersService;
    }

    @GetMapping
    public List<UserInfo> getAll(@RequestParam(required = false) String name) {
        if (name != null && !name.isEmpty()) {
            return service.getByUsername(name);
        }
        return service.getAll();
    }

    @GetMapping("/{id}")
    public UserInfo getById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public UserInfo create(@RequestBody @Valid UserDto userDto) {
        try {
            UserInfo newUserInfo = mapper.fromDto(userDto);
            return service.create(newUserInfo);
        } catch (DuplicateEntityException dee) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, dee.getMessage());
        } catch (EntityNotFoundException nfe) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, nfe.getMessage());
        }
    }

    @PutMapping("/{id}")
    public UserInfo update(@PathVariable int id, @RequestBody UserDto userDto) {
        try {
            UserInfo newUserInfo = mapper.fromDto(userDto);
            newUserInfo.setId(id);
            service.update(newUserInfo);
            return newUserInfo;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            service.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}/wish-list")
    public Set<Beer> getWishList(@PathVariable int id) {
        try {
            return service.getWishList(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
