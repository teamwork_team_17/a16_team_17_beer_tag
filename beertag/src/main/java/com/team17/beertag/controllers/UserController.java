package com.team17.beertag.controllers;

import com.team17.beertag.models.UserInfo;
import com.team17.beertag.services.BeersService;
import com.team17.beertag.services.RatingsService;
import com.team17.beertag.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class UserController {
    private UsersService service;
    private RatingsService ratingsService;

    @Autowired
    public UserController(UsersService service, RatingsService ratingsService) {
        this.service = service;
        this.ratingsService = ratingsService;
    }

    @GetMapping("/profile")
    public String showUserInformation(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo user = service.getByUsername(authentication.getName()).get(0);
        model.addAttribute("user", user);
        model.addAttribute("beerWishList", service.getWishList(user.getId()));
        model.addAttribute("beerDrankList", service.getDrankList(user.getId()));
        model.addAttribute("beerTopList", ratingsService.getTOPBeersOfUser(user.getId()));
        return "profile";
    }
}
