package com.team17.beertag.controllers.RESTcontrollers;

import com.team17.beertag.exceptions.DuplicateEntityException;
import com.team17.beertag.exceptions.EntityNotFoundException;
import com.team17.beertag.models.Beer;
import com.team17.beertag.models.Brewery;
import com.team17.beertag.services.BreweriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/breweries")
public class BreweriesControllerREST {
    private BreweriesService breweriesService;

    @Autowired
    public BreweriesControllerREST(BreweriesService breweriesService) {
        this.breweriesService = breweriesService;
    }

    @GetMapping
    public List<Brewery> getAll() {
        return breweriesService.getAll();
    }

    @GetMapping("/{id}")
    public Brewery getById(@PathVariable int id) {
        try {
            return breweriesService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{breweryId}/beers")
    public List<Beer> getBreweryBeers(@PathVariable int breweryId) {
        try {
            return breweriesService.getBreweryBeers(breweryId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{name}")
    public List<Brewery> getByName(@PathVariable String name) {
        try {
            return breweriesService.getByName(name);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Brewery create(@RequestBody @Valid Brewery brewery) {
        try {
            Brewery newBrewery = new Brewery();
            newBrewery.setName(brewery.getName());
            return breweriesService.create(newBrewery);
        } catch (DuplicateEntityException dee) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, dee.getMessage());
        } catch (EntityNotFoundException nfe) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, nfe.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Brewery update(@PathVariable int id, @RequestBody @Valid Brewery brewery) {
        try {
            Brewery breweryToUpdate = new Brewery();
            breweryToUpdate.setId(id);
            breweryToUpdate.setName(brewery.getName());
            breweriesService.update(breweryToUpdate);
            return breweryToUpdate;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            breweriesService.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
