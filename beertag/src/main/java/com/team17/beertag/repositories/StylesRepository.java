package com.team17.beertag.repositories;

import com.team17.beertag.models.Beer;
import com.team17.beertag.models.Style;

import java.util.List;

public interface StylesRepository {
    Style create(Style style);

    List<Style> getAll();

    Style getById(int id);

    List<Style> getByName(String name);

    boolean checkStyleExists(String name);

    void update(Style style);

    void delete(int id);

}
