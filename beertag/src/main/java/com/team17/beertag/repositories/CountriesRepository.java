package com.team17.beertag.repositories;

import com.team17.beertag.models.Country;

import java.util.List;

public interface CountriesRepository {
    Country create(Country country);

    List<Country> getAll();

    Country getById(int id);

    List<Country> getByName(String name);

    boolean checkCountryExists(String name);

    void update(Country country);

    void delete(int id);
}
