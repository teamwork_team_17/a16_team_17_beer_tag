package com.team17.beertag.repositories;

import com.team17.beertag.exceptions.EntityNotFoundException;
import com.team17.beertag.models.Beer;
import com.team17.beertag.models.Style;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class StylesRepositoryImpl implements StylesRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public StylesRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Style create(Style style) {
        try(Session session = sessionFactory.openSession()){
            session.save(style);
        }
        return style;
    }

    @Override
    public List<Style> getAll() {
        try(Session session = sessionFactory.openSession()){
            return session.createQuery("from Style", Style.class).list();
        }
    }

    @Override
    public Style getById(int id) {
        try(Session session = sessionFactory.openSession()){
            return getById(id, session);
        }
    }

    @Override
    public List<Style> getByName(String name) {
        try(Session session = sessionFactory.openSession()){
            Query<Style> query = session.createQuery("from Style where name = :name", Style.class);
            query.setParameter("name", "%" + name + "%");
            return query.list();
        }
    }

    @Override
    public boolean checkStyleExists(String name) {
        return getByName(name).size()!= 0;
    }

    @Override
    public void update(Style style) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(style);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        try(Session session = sessionFactory.openSession()){
            Style style = getById(id);
            session.beginTransaction();
            session.delete(style);
            session.getTransaction().commit();
        }
    }

    private Style getById(int id, Session session) {
        Style style = session.get(Style.class, id);
        if(style==null){
            throw new EntityNotFoundException("Style", id);
        }
        return style;
    }
}