package com.team17.beertag.repositories;

import com.team17.beertag.models.Rating;

import java.util.List;

public interface RatingsRepository {
    Rating create(Rating rating);

    void update(Rating rating);

    void delete(Rating rating);

    double getAverageRatingForBeer(int beerId);

    List<Rating> getBeersOfUser(int userId);
}
