package com.team17.beertag.repositories;

import com.team17.beertag.exceptions.EntityNotFoundException;
import com.team17.beertag.models.Beer;
import com.team17.beertag.models.UserInfo;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class BeersRepositoryImpl implements BeersRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public BeersRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Beer create(Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            session.save(beer);
        }
        return beer;
    }

    @Override
    public List<Beer> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("from Beer", Beer.class);
            return query.list();
        }
    }

    @Override
    public List<Beer> getFilteredAndSorted(Integer country, Integer style, String sortBy) {

        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Beer> cr = cb.createQuery(Beer.class);
        Root<Beer> root = cr.from(Beer.class);
        //cr.select(root);
        //Predicate[] predicates = new Predicate[2];
        //Predicate[] predicates = new Predicate[2];
        List<Predicate> predicates = new ArrayList<>();

        if(country!=null) {
            predicates.add(cb.equal(root.get("country"), country));
        }
        if(style!=null) {
            //cr.select(root).where(cb.equal(root.get("style"), style));

            predicates.add(cb.equal(root.get("style"), style));
        }
        cr.select(root).where(predicates.toArray(new Predicate[0]));
        if(sortBy!=null) {
            if (sortBy.equals("name")) {
                cr.orderBy(cb.asc(root.get("name")));
            }
            if (sortBy.equals("named")) {
                cr.orderBy(cb.desc(root.get("name")));
            }
            if (sortBy.equals("abv")) {
                cr.orderBy(cb.asc(root.get("abv")));
            }
            if (sortBy.equals("abvd")) {
                cr.orderBy(cb.desc(root.get("abv")));
            }
        }
        Query<Beer> query = session.createQuery(cr);
        return query.getResultList();


    }

    @Override
    public Beer getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Beer beer = session.get(Beer.class, id);
            if (beer == null) {
                throw new EntityNotFoundException("Beer", id);
            }
            return beer;
        }
    }

    @Override
    public List<Beer> getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("from Beer where name LIKE :name", Beer.class);
            query.setParameter("name", "%" + name + "%");
            return query.list();
        }
    }

    @Override
    public List<Beer> getByStyle(int styleId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("from Beer where style.id = :styleId", Beer.class);
            query.setParameter("styleId", styleId);
            return query.list();
        }
    }

    @Override
    public List<Beer> getByCountry(int countryId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("from Beer where country.id = :countryId", Beer.class);
            query.setParameter("countryId", countryId);
            return query.list();
        }
    }

    @Override
    public List<Beer> getByBrewery(int breweryId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("from Beer where brewery.id = :breweryId", Beer.class);
            query.setParameter("breweryId", breweryId);
            return query.list();
        }
    }

    @Override
    public List<Beer> getByTag(int tagId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("select b from Beer b join b.tags t where t.id=:tagId", Beer.class);
            query.setParameter("tagId", tagId);
            return query.list();
        }
    }

    @Override
    public List<Beer> sortBy(String criteria) {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("from Beer order by" + criteria, Beer.class);

            return query.list();
        }
    }

    @Override
    public List<Beer> createdBy(UserInfo createdBy) {
        try (Session session = sessionFactory.openSession()){
            Query<Beer> query = session.createQuery("from Beer where createdBy = :createdBy", Beer.class);
            query.setParameter("createdBy", createdBy);
            return query.list();
        }
    }

    @Override
    public boolean checkBeerExists(String name) {
        return getByName(name).size() != 0;
    }

    @Override
    public void update(Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(beer);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        try (Session session = sessionFactory.openSession()) {
            Beer beer = getById(id);
            session.beginTransaction();
            session.delete(beer);
            session.getTransaction().commit();
        }
    }

    @Override
    public void addBeerToWishList(int beerId, int userId) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Beer beer = getById(beerId);
            UserInfo user = session.get(UserInfo.class, userId);
            user.getWishList().add(beer);
            session.getTransaction().commit();
        }
    }

    @Override
    public void addBeerToDrankList(int beerId, int userId) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Beer beer = getById(beerId);
            UserInfo user = session.get(UserInfo.class, userId);
            user.getDrankList().add(beer);
            beer.getUserDrankList().add(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void deleteBeerFromWishList(int beerId, int userId) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Beer beer = session.get(Beer.class, beerId);
            UserInfo user = session.get(UserInfo.class, userId);
            user.getWishList().remove(beer);
            session.getTransaction().commit();
        }
    }

    @Override
    public void deleteBeerFromDrankList(int beerId, int userId) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Beer beer = session.get(Beer.class, beerId);
            UserInfo user = session.get(UserInfo.class, userId);
            user.getDrankList().remove(beer);
            session.getTransaction().commit();
        }
    }
}
