package com.team17.beertag.repositories;

import com.team17.beertag.exceptions.EntityNotFoundException;
import com.team17.beertag.models.Tag;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TagsRepositoryImpl implements TagsRepository {
    SessionFactory sessionFactory;

    @Autowired
    public TagsRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Tag create(Tag tag) {
        try(Session session = sessionFactory.openSession()){
            session.save(tag);
            return tag;
        }
    }

    @Override
    public List<Tag> getAll() {
        try(Session session = sessionFactory.openSession()){
            return session.createQuery("from Tag", Tag.class).list();
        }
    }

    @Override
    public Tag getById(int id) {
        try(Session session = sessionFactory.openSession()){
            return getById(id, session);
        }
    }

    @Override
    public List<Tag> getByName(String name) {
        try(Session session = sessionFactory.openSession()){
            Query<Tag> query = session.createQuery("from Tag where name = :name", Tag.class);
            query.setParameter("name", "%" + name + "%");
            return query.list();
        }
    }

    @Override
    public boolean checkTagExists(String name) {
        return getByName(name).size() != 0;
    }

    @Override
    public void update(Tag tag) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(tag);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        try(Session session = sessionFactory.openSession()){
            Tag tag = getById(id);
            session.beginTransaction();
            session.delete(tag);
            session.getTransaction().commit();
        }
    }

    private Tag getById(int id, Session session) {
        Tag tag = session.get(Tag.class, id);
        if (tag == null) {
            throw new EntityNotFoundException("Tag", id);
        }
        return tag;
    }
}
