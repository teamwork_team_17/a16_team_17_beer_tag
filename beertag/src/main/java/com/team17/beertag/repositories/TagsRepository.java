package com.team17.beertag.repositories;

import com.team17.beertag.models.Tag;

import java.util.List;

public interface TagsRepository {
    Tag create(Tag tag);

    List<Tag> getAll();

    Tag getById(int id);

    List<Tag> getByName(String name);

    boolean checkTagExists(String name);

    void update(Tag tag);

    void delete(int id);
}
