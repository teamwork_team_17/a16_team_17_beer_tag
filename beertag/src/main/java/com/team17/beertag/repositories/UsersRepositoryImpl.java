package com.team17.beertag.repositories;

import com.team17.beertag.exceptions.EntityNotFoundException;
import com.team17.beertag.models.Beer;
import com.team17.beertag.models.User;
import com.team17.beertag.models.UserInfo;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public class UsersRepositoryImpl implements UsersRepository {
    SessionFactory sessionFactory;

    @Autowired
    public UsersRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public UserInfo create(UserInfo userInfo) {
        try (Session session = sessionFactory.openSession()) {
            session.save(userInfo);
        }
        return userInfo;
    }

    @Override
    public List<UserInfo> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<UserInfo> query = session.createQuery("from UserInfo", UserInfo.class);
            return query.list();
        }
    }

    @Override
    public UserInfo getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            UserInfo userInfo = session.get(UserInfo.class, id);
            if (userInfo == null) {
                throw new EntityNotFoundException("User", id);
            }
            return userInfo;
        }
    }

    @Override
    public List<UserInfo> getByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserInfo> query = session.createQuery("from UserInfo where username like :username", UserInfo.class);
            query.setParameter("username", "%" + username + "%");
            return query.list();
        }
    }

    @Override
    public boolean checkUserExists(String username) {
        return getByUsername(username).size() != 0;
    }

    @Override
    public void update(UserInfo userInfo) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(userInfo);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        try (Session session = sessionFactory.openSession()) {
            UserInfo userInfo = getById(id);
            session.beginTransaction();
            session.delete(userInfo);
            session.getTransaction().commit();
        }
    }

    @Override
    public Set<Beer> getWishList(int id) {
        try (Session session = sessionFactory.openSession()) {
            UserInfo user = session.get(UserInfo.class, id);
            return user.getWishList();
        }
    }

    @Override
    public Set<Beer> getDrankList(int id) {
        try (Session session = sessionFactory.openSession()) {
            UserInfo user = session.get(UserInfo.class, id);
            return user.getDrankList();
        }
    }

    @Override
    public Set<UserInfo> getAllUsersBeerInWishList(int beerId) {
        try (Session session = sessionFactory.openSession()) {
            Beer beer = session.get(Beer.class, beerId);
            return beer.getUserWishList();
        }
    }

    @Override
    public Set<UserInfo> getAllUsersBeerInDrankList(int beerId) {
        try (Session session = sessionFactory.openSession()) {
            Beer beer = session.get(Beer.class, beerId);
            return beer.getUserDrankList();
        }
    }
}
