package com.team17.beertag.repositories;

import com.team17.beertag.models.Beer;
import com.team17.beertag.models.UserInfo;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface BeersRepository {
    Beer create(Beer beer);

    List<Beer> getAll();

    List<Beer> getFilteredAndSorted(Integer country, Integer style, String sortBy);

    Beer getById(int id);

    List<Beer> getByName(String name);

    List<Beer> getByStyle(int styleId);

    List<Beer> getByCountry(int countryId);

    List<Beer> getByBrewery(int breweryId);

    List<Beer> getByTag(int tagId);

    List<Beer> sortBy(String criteria);

    List<Beer> createdBy(UserInfo createdBy);

    boolean checkBeerExists(String name);

    void update(Beer beer);

    void delete(int id);

    void addBeerToWishList(int beerId, int userId);

    void addBeerToDrankList(int beerId, int userId);

    void deleteBeerFromWishList(int beerId, int userId);

    void deleteBeerFromDrankList(int beerId, int userId);
}


