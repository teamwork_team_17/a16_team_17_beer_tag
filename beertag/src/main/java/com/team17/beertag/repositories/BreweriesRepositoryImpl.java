package com.team17.beertag.repositories;

import com.team17.beertag.exceptions.EntityNotFoundException;
import com.team17.beertag.models.Brewery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BreweriesRepositoryImpl implements BreweriesRepository {
    SessionFactory sessionFactory;

    @Autowired
    public BreweriesRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Brewery create(Brewery brewery) {
        try (Session session = sessionFactory.openSession()) {
            session.save(brewery);
        }
        return brewery;
    }

    @Override
    public List<Brewery> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Brewery", Brewery.class).list();
        }
    }

    @Override
    public Brewery getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            return getById(id, session);
        }
    }

    @Override
    public List<Brewery> getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Brewery> query = session.createQuery("from Brewery where name = :name", Brewery.class);
            query.setParameter("name", "%" + name + "%");
            return query.list();
        }
    }

    @Override
    public boolean checkBreweryExists(String name) {
        return getByName(name).size() != 0;
    }

    @Override
    public void update(Brewery brewery) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(brewery);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        try(Session session = sessionFactory.openSession()){
            Brewery brewery = getById(id);
            session.beginTransaction();
            session.delete(brewery);
            session.getTransaction().commit();
        }
    }

    private Brewery getById(int id, Session session) {
        Brewery brewery = session.get(Brewery.class, id);
        if (brewery == null) {
            throw new EntityNotFoundException("Brewery", id);
        }
        return brewery;
    }
}
