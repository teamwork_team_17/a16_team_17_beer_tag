package com.team17.beertag.repositories;

import com.team17.beertag.models.Beer;
import com.team17.beertag.models.UserInfo;

import java.util.List;
import java.util.Set;

public interface UsersRepository {
    UserInfo create(UserInfo userInfo);

    List<UserInfo> getAll();

    UserInfo getById(int id);

    List<UserInfo> getByUsername(String username);

    boolean checkUserExists(String username);

    void update(UserInfo userInfo);

    void delete(int id);

    Set<Beer> getWishList(int id);

    Set<Beer> getDrankList(int id);

    Set<UserInfo> getAllUsersBeerInWishList(int beerId);

    Set<UserInfo> getAllUsersBeerInDrankList(int beerId);
}
