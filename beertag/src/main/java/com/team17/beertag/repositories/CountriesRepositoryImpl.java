package com.team17.beertag.repositories;

import com.team17.beertag.exceptions.EntityNotFoundException;
import com.team17.beertag.models.Country;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CountriesRepositoryImpl implements CountriesRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public CountriesRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Country create(Country country) {
        try(Session session = sessionFactory.openSession()){
            session.save(country);
        }
        return country;
    }

    @Override
    public List<Country> getAll() {
        try(Session session = sessionFactory.openSession()){
            Query<Country> query = session.createQuery("from Country ", Country.class);
            return query.list();
        }
    }

    @Override
    public Country getById(int id) {
        try(Session session = sessionFactory.openSession()){
            return getById(id, session);
        }
    }

    @Override
    public List<Country> getByName(String name) {
        try(Session session = sessionFactory.openSession()){
            Query<Country> query = session.createQuery("from Country where name = :name", Country.class);
            query.setParameter("name", "%" + name + "%");
            return query.list();
        }
    }

    @Override
    public boolean checkCountryExists(String name) {
        return getByName(name).size()!= 0;
    }

    @Override
    public void update(Country country) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(country);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        try(Session session = sessionFactory.openSession()){
            Country country = getById(id);
            session.beginTransaction();
            session.delete(country);
            session.getTransaction().commit();
        }
    }

    private Country getById(int id, Session session) {
        Country country = session.get(Country.class, id);
        if(country==null){
            throw new EntityNotFoundException("Country", id);
        }
        return country;
    }
}
