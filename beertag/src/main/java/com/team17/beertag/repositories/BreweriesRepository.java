package com.team17.beertag.repositories;

import com.team17.beertag.models.Brewery;

import java.util.List;

public interface BreweriesRepository {
    Brewery create(Brewery brewery);

    List<Brewery> getAll();

    Brewery getById(int id);

    List<Brewery> getByName(String name);

    boolean checkBreweryExists(String name);

    void update(Brewery brewery);

    void delete(int id);
}
