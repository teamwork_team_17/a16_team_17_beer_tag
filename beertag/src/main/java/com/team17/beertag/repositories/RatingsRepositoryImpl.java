package com.team17.beertag.repositories;

import com.team17.beertag.models.Beer;
import com.team17.beertag.models.Rating;
import com.team17.beertag.models.UserInfo;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RatingsRepositoryImpl implements RatingsRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public RatingsRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Rating create(Rating rating) {
        try (Session session = sessionFactory.openSession()) {
            session.save(rating);
            return rating;
        }
    }

    @Override
    public void update(Rating rating) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(rating);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(Rating rating) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(rating);
            session.getTransaction().commit();
        }
    }

    @Override
    public double getAverageRatingForBeer(int beerId) {
        try (Session session = sessionFactory.openSession()) {
            Beer beer = session.get(Beer.class, beerId);
            double averageRating = (double) session.createQuery("select avg(rating) from Rating where beerId = :beerId")
                    .setParameter("beerId", beer).getSingleResult();
            return averageRating;
        }
    }

    @Override
    public List<Rating> getBeersOfUser(int userId) {
        try (Session session = sessionFactory.openSession()) {
            UserInfo user = session.get(UserInfo.class, userId);
            Query<Rating> query = session.createQuery("from Rating where userInfoId = :user", Rating.class);
            query.setParameter("user", user);

            return query.list();
        }
    }

}
