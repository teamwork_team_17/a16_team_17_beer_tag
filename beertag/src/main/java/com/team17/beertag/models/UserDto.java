package com.team17.beertag.models;

import javax.validation.constraints.*;

public class UserDto {

    @NotBlank
    @Size(min = 5, max = 20, message = "Username should be between 5 and 20 symbols.")
    private String username;

    @NotBlank
    @Size(min = 8, max = 30, message = "Password should be between 8 and 30 symbols.")
    private String password;

    @NotBlank
    private String email;

    public UserDto() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
