package com.team17.beertag.models;

import org.springframework.stereotype.Component;

@Component
public class UserDtoMapper {
    public UserInfo fromDto(UserDto userDto){
        UserInfo userInfo = new UserInfo();
        userInfo.setUsername(userDto.getUsername());
        userInfo.setPassword(userDto.getPassword());
        userInfo.setEmail(userDto.getEmail());
        return userInfo;
    }
}
