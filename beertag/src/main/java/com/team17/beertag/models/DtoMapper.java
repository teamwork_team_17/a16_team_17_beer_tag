package com.team17.beertag.models;

import com.team17.beertag.services.BreweriesService;
import com.team17.beertag.services.CountriesService;
import com.team17.beertag.services.StylesService;
import com.team17.beertag.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DtoMapper {
    private StylesService stylesService;
    private CountriesService countriesService;
    private BreweriesService breweriesService;
    private UsersService usersService;

    @Autowired
    public DtoMapper(StylesService stylesService,
                     CountriesService countriesService,
                     BreweriesService breweriesService,
                     UsersService usersService){
        this.stylesService = stylesService;
        this.countriesService = countriesService;
        this.breweriesService = breweriesService;
        this.usersService = usersService;
    }

    public Beer fromDto(BeerDto beerDto){
        Beer beer = new Beer();
        beer.setName(beerDto.getName());
        beer.setAbv(beerDto.getAbv());
        Style style = stylesService.getById(beerDto.getStyleId());
        beer.setStyle(style);
        Country country = countriesService.getById(beerDto.getCountryId());
        beer.setCountry(country);
        Brewery brewery = breweriesService.getById(beerDto.getBreweryId());
        beer.setBrewery(brewery);
        return beer;
    }
}
