package com.team17.beertag.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.tomcat.util.codec.binary.Base64;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
@Table(name = "beers")
public class Beer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "beer_id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "abv")
    private double abv;

    @ManyToOne
    @JoinColumn(name = "style_id")
    private Style style;

    @ManyToOne
    @JoinColumn(name = "brewery_id")
    private Brewery brewery;

    @ManyToOne
    @JoinColumn(name = "country_id")
    private Country country;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "beerId", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Rating> ratings = new ArrayList<>();

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(
            name = "beerstags",
            joinColumns = @JoinColumn(name = "beer_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id")
    )
    private Set<Tag> tags;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserInfo createdBy;

    @Lob
    @Column(name = "image")
    private byte[] image;

    @ManyToMany (mappedBy = "wishList", fetch = FetchType.EAGER)
    private Set<UserInfo> userWishList;

    @ManyToMany (mappedBy = "drankList", fetch = FetchType.EAGER)
    private Set<UserInfo> userDrankList;

    public Beer() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getAbv() {
        return abv;
    }

    public void setAbv(double abv) {
        this.abv = abv;
    }

    public Style getStyle() {
        return style;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    public Brewery getBrewery() {
        return brewery;
    }

    public void setBrewery(Brewery brewery) {
        this.brewery = brewery;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public UserInfo getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(UserInfo createdBy) {
        this.createdBy = createdBy;
    }

    public List<Rating> getRatings() {
        return ratings;
    }

    public void setRatings(List<Rating> ratings) {
        this.ratings = ratings;
    }

    public void addRating(Rating rating) {
        ratings.add(rating);
    }

    public void deleteRating(Rating rating) {
        ratings.remove(rating);
    }

    public Double getAverageRating() {
        double sum = 0.0;

        for (int i = 0; i < ratings.size(); i++) {
            sum += ratings.get(i).getRating();
        }

        if (ratings.size() != 0) {
            return sum / ratings.size();
        } else {
            return 0.0;
        }
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String generateBase64Image() {
        return Base64.encodeBase64String(this.getImage());
    }

    public Set<UserInfo> getUserDrankList() {
        return userDrankList;
    }

    public void setUserDrankList(Set<UserInfo> userDrankList) {
        this.userDrankList = userDrankList;
    }

    public Set<UserInfo> getUserWishList() {
        return userWishList;
    }

    public void setUserWishList(Set<UserInfo> userWishList) {
        this.userWishList = userWishList;
    }
}
