package com.team17.beertag.models;

import javax.validation.constraints.*;

public class BeerDto {
    @NotBlank
    @NotNull
    @Size(min = 2, max = 25,message = "Name should be between 2 and 25 symbols")
    private String name;

    @PositiveOrZero
    private double abv;

    @PositiveOrZero(message = "StyleId should be positive or zero")
    private int styleId;

    @PositiveOrZero(message = "BreweryId should be positive or zero")
    private int breweryId;

    @PositiveOrZero(message = "CountryId should be positive or zero")
    private int countryId;

    @PositiveOrZero(message = "UserId should be positive or zero")
    private int userId;

    private byte[] image;

    private String description;

    public BeerDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAbv() {
        return abv;
    }

    public void setAbv(double abv) {
        this.abv = abv;
    }

    public int getStyleId() {
        return styleId;
    }

    public void setStyleId(int styleId) {
        this.styleId = styleId;
    }

    public int getBreweryId() {
        return breweryId;
    }

    public void setBreweryId(int breweryId) {
        this.breweryId = breweryId;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
