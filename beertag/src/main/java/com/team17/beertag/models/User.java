package com.team17.beertag.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.Size;
import java.io.Serializable;

public class User implements Serializable {

    @Size(min = 4, message = "Username is required")
    private String username;

    @Size(min = 4, message = "Password is required")
    private String password;

    @Size(min = 4, message = "Password is required")
    private String confirmPassword;

    public User() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}