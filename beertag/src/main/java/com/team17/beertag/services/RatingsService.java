package com.team17.beertag.services;

import com.team17.beertag.models.Beer;
import com.team17.beertag.models.Rating;

import java.util.List;

public interface RatingsService {
    Rating create(Rating rating);

    void update(Rating rating);

    void delete(Rating rating);

    double getAverageRatingForBeer(int beerId);

    List<Rating> getBeersOfUser(int userId);

    List<Beer> getTOPBeersOfUser(int userId);
}
