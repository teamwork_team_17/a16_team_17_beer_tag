package com.team17.beertag.services;

import com.team17.beertag.exceptions.DuplicateEntityException;
import com.team17.beertag.models.Beer;
import com.team17.beertag.models.Style;
import com.team17.beertag.repositories.BeersRepository;
import com.team17.beertag.repositories.StylesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StylesServiceImpl implements StylesService {
    private StylesRepository stylesRepository;
    private BeersRepository beersRepository;

    @Autowired
    public StylesServiceImpl(StylesRepository stylesRepository, BeersRepository beersRepository){
        this.stylesRepository = stylesRepository;
        this.beersRepository = beersRepository;
    }

    @Override
    public Style create(Style style) {
        if(stylesRepository.checkStyleExists(style.getName())){
            throw new DuplicateEntityException(String.format("Style with name %s already exists", style.getName()));
        }
        return stylesRepository.create(style);
    }

    @Override
    public List<Style> getAll() {
        return stylesRepository.getAll();
    }

    @Override
    public Style getById(int id) {
        return stylesRepository.getById(id);
    }

    @Override
    public List<Beer> getStyleBeers(int styleId) {
        return beersRepository.getByStyle(styleId);
    }

    @Override
    public void update(Style style) {
        stylesRepository.update(style);
    }

    @Override
    public void delete(int id) {
        stylesRepository.delete(id);
    }
}
