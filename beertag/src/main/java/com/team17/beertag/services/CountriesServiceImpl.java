package com.team17.beertag.services;

import com.team17.beertag.exceptions.DuplicateEntityException;
import com.team17.beertag.models.Country;
import com.team17.beertag.repositories.CountriesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountriesServiceImpl implements CountriesService {
    private CountriesRepository countriesRepository;

    @Autowired
    public CountriesServiceImpl(CountriesRepository countriesRepository) {
        this.countriesRepository = countriesRepository;
    }

    @Override
    public Country create(Country country) {
        if(countriesRepository.checkCountryExists(country.getName())){
            throw new DuplicateEntityException(String.format("Country with name %s already exists", country.getName()));
        }
        return countriesRepository.create(country);
    }

    @Override
    public List<Country> getAll() {
        return countriesRepository.getAll();
    }

    @Override
    public Country getById(int id) {
        return countriesRepository.getById(id);
    }

    @Override
    public List<Country> getByName(String name) {
        return countriesRepository.getByName(name);
    }

    @Override
    public void update(Country country) {
        countriesRepository.update(country);
    }

    @Override
    public void delete(int id) {
        countriesRepository.delete(id);
    }
}
