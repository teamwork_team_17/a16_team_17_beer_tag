package com.team17.beertag.services;

import com.team17.beertag.exceptions.DuplicateEntityException;
import com.team17.beertag.models.Beer;
import com.team17.beertag.models.Brewery;
import com.team17.beertag.repositories.BeersRepository;
import com.team17.beertag.repositories.BreweriesRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BreweriesServiceImpl implements BreweriesService {
    BreweriesRepository breweriesRepository;
    BeersRepository beersRepository;

    public BreweriesServiceImpl(BreweriesRepository breweriesRepository, BeersRepository beersRepository) {
        this.breweriesRepository = breweriesRepository;
        this.beersRepository = beersRepository;
    }

    @Override
    public Brewery create(Brewery brewery) {
        if (breweriesRepository.checkBreweryExists(brewery.getName())) {
            throw new DuplicateEntityException(String.format("Brewery with name %s already exists", brewery.getName()));
        }
        return breweriesRepository.create(brewery);
    }

    @Override
    public List<Brewery> getAll() {
        return breweriesRepository.getAll();
    }

    @Override
    public Brewery getById(int id) {
        return breweriesRepository.getById(id);
    }

    @Override
    public List<Beer> getBreweryBeers(int breweryId) {
        return beersRepository.getByBrewery(breweryId);
    }

    @Override
    public List<Brewery> getByName(String name) {
        return breweriesRepository.getByName(name);
    }

    @Override
    public void update(Brewery brewery) {
        breweriesRepository.update(brewery);
    }

    @Override
    public void delete(int id) {
        breweriesRepository.delete(id);
    }
}
