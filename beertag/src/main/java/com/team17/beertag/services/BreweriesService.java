package com.team17.beertag.services;

import com.team17.beertag.models.Beer;
import com.team17.beertag.models.Brewery;
import com.team17.beertag.models.Tag;

import java.util.List;

public interface BreweriesService {
    Brewery create(Brewery brewery);

    List<Brewery> getAll();

    Brewery getById(int id);

    List<Beer> getBreweryBeers(int breweryId);

    List<Brewery> getByName(String name);

    void update(Brewery brewery);

    void delete(int id);
}
