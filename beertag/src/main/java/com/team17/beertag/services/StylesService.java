package com.team17.beertag.services;

import com.team17.beertag.models.Beer;
import com.team17.beertag.models.Style;

import java.util.List;

public interface StylesService {
    Style create(Style style);

    List<Style> getAll();

    Style getById(int id);

    List<Beer> getStyleBeers(int styleId);

    void update(Style style);

    void delete(int id);
}
