package com.team17.beertag.services;

import com.team17.beertag.exceptions.DuplicateEntityException;
import com.team17.beertag.exceptions.InvalidOperationException;
import com.team17.beertag.models.Beer;
import com.team17.beertag.models.UserInfo;
import com.team17.beertag.repositories.BeersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BeersServiceImpl implements BeersService {

    private BeersRepository repository;

    @Autowired
    public BeersServiceImpl(BeersRepository repository) {
        this.repository = repository;
    }

    @Override
    public Beer create(Beer beer) {
        if (repository.checkBeerExists(beer.getName())) {
            throw new DuplicateEntityException(String.format("Beer with name %s already exists", beer.getName()));
        }
        return repository.create(beer);
    }

    @Override
    public List<Beer> getAll() {
        return repository.getAll();
    }

    @Override
    public List<Beer> getFilteredAndSorted(Integer country, Integer style, String sortBy) {
        return repository.getFilteredAndSorted(country, style, sortBy);
    }

    @Override
    public Beer getById(int id) {
        return repository.getById(id);
    }

    @Override
    public List<Beer> getByName(String name) {
        return repository.getByName(name);
    }

    @Override
    public List<Beer> getByStyle(int styleId) {
        return repository.getByStyle(styleId);
    }

    @Override
    public List<Beer> getByCountry(int countryId) {
        return repository.getByCountry(countryId);
    }

    @Override
    public List<Beer> getByTag(int tagId) {
        return repository.getByTag(tagId);
    }

    @Override
    public List<Beer> sortBy(String criteria) {
        return repository.sortBy(criteria);
    }

    @Override
    public List<Beer> createdBy(UserInfo createdBy) {
        return repository.createdBy(createdBy);
    }

    @Override
    public void update(Beer beer, UserInfo userInfo) {
        if (beer.getCreatedBy().getId() != userInfo.getId()
                && !userInfo.getRoles().stream().anyMatch(role -> role.getName().equals("Administrator"))) {
            throw new InvalidOperationException(
                    String.format("User %s cannot modify beer with id %d", userInfo.getUsername(), beer.getId()));
        }


        repository.update(beer);
    }

    @Override
    public void delete(int id, UserInfo userInfo) {
        Beer beer = repository.getById(id);
        if (beer.getCreatedBy().getId() != userInfo.getId()
                && !userInfo.getRoles().stream().anyMatch(role -> role.getName().equals("Administrator"))) {
            throw new InvalidOperationException(
                    String.format("User %s cannot delete beer with id %d", userInfo.getUsername(), beer.getId()));
        }

        repository.delete(id);
    }

    @Override
    public void addBeerToWishList(int beerId, int userId) {
        repository.addBeerToWishList(beerId, userId);
    }

    @Override
    public void addBeerToDrankList(int beerId, int userId) {
        repository.addBeerToDrankList(beerId, userId);
    }

    @Override
    public void deleteBeerFromWishList(int beerId, int userId) {
        repository.deleteBeerFromWishList(beerId, userId);
    }

    @Override
    public void deleteBeerFromDrankList(int beerId, int userId) {
        repository.deleteBeerFromDrankList(beerId, userId);
    }
}