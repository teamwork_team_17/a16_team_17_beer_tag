package com.team17.beertag.services;

import com.team17.beertag.models.Beer;
import com.team17.beertag.models.UserInfo;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface BeersService {
    Beer create(Beer beer);

    List<Beer> getAll();

    List<Beer> getFilteredAndSorted(Integer country, Integer style, String sortBy);

    Beer getById(int id);

    List<Beer> getByName(String name);

    List<Beer> getByStyle(int styleId);

    List<Beer> getByCountry(int countryId);

    List<Beer> getByTag(int tagId);

    List<Beer> sortBy(String criteria);

    List<Beer> createdBy(UserInfo createdBy);

    void update(Beer beer, UserInfo userInfo);

    void delete(int id, UserInfo userInfo);

    void addBeerToWishList(int beerId, int userId);

    void addBeerToDrankList(int beerId, int userId);

    void deleteBeerFromWishList(int beerId, int userId);

    void deleteBeerFromDrankList(int beerId, int userId);
}
