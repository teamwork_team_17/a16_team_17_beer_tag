package com.team17.beertag.services;

import com.team17.beertag.models.Beer;
import com.team17.beertag.models.UserInfo;

import java.util.List;
import java.util.Set;

public interface UsersService {
    UserInfo create(UserInfo userInfo);

    List<UserInfo> getAll();

    UserInfo getById(int id);

    List<UserInfo> getByUsername(String username);

    void update(UserInfo userInfo);

    void delete(int id);

    Set<Beer> getWishList(int id);

    Set<Beer> getDrankList(int id);

    Set<UserInfo> getAllUsersBeerInWishList(int beerId);

    Set<UserInfo> getAllUsersBeerInDrankList(int beerId);
}
