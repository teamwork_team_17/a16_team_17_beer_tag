package com.team17.beertag.services;

import com.team17.beertag.exceptions.DuplicateEntityException;
import com.team17.beertag.models.Beer;
import com.team17.beertag.models.UserInfo;
import com.team17.beertag.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class UsersServiceImpl implements UsersService {

    private UsersRepository repository;

    @Autowired
    public UsersServiceImpl(UsersRepository repository) {
        this.repository = repository;
    }

    @Override
    public UserInfo create(UserInfo userInfo) {
        if (repository.checkUserExists(userInfo.getUsername())) {
            throw new DuplicateEntityException(String.format("User with name %s already exists", userInfo.getUsername()));
        }
        return repository.create(userInfo);
    }

    @Override
    public List<UserInfo> getAll() {
        return repository.getAll();
    }

    @Override
    public UserInfo getById(int id) {
        return repository.getById(id);
    }

    @Override
    public List<UserInfo> getByUsername(String username) {
        return repository.getByUsername(username);
    }

    @Override
    public void update(UserInfo userInfo) {
        repository.update(userInfo);
    }

    @Override
    public void delete(int id) {
        repository.delete(id);
    }

    @Override
    public Set<Beer> getWishList(int id) {
        return repository.getWishList(id);
    }

    @Override
    public Set<Beer> getDrankList(int id) {
        return repository.getDrankList(id);
    }

    @Override
    public Set<UserInfo> getAllUsersBeerInWishList(int beerId) {
        return repository.getAllUsersBeerInWishList(beerId);
    }

    @Override
    public Set<UserInfo> getAllUsersBeerInDrankList(int beerId) {
        return repository.getAllUsersBeerInDrankList(beerId);
    }
}
