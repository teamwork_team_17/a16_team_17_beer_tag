package com.team17.beertag.services;

import com.team17.beertag.exceptions.DuplicateEntityException;
import com.team17.beertag.models.Tag;
import com.team17.beertag.repositories.TagsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagsServiceImpl implements TagsService {
    private TagsRepository tagsRepository;

    @Autowired
    public TagsServiceImpl(TagsRepository tagsRepository) {
        this.tagsRepository = tagsRepository;
    }

    @Override
    public Tag create(Tag tag) {
        if (tagsRepository.checkTagExists(tag.getName())) {
            throw new DuplicateEntityException(String.format("Tag with name %s already exists", tag.getName()));
        }
        return tagsRepository.create(tag);
    }

    @Override
    public List<Tag> getAll() {
        return tagsRepository.getAll();
    }

    @Override
    public Tag getById(int id) {
        return tagsRepository.getById(id);
    }

    @Override
    public List<Tag> getByName(String name) {
        return tagsRepository.getByName(name);
    }

    @Override
    public void update(Tag tag) {
        tagsRepository.update(tag);
    }

    @Override
    public void delete(int id) {
        tagsRepository.delete(id);
    }
}
