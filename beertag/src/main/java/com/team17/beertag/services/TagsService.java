package com.team17.beertag.services;

import com.team17.beertag.models.Tag;

import java.util.List;

public interface TagsService {
    Tag create(Tag tag);

    List<Tag> getAll();

    Tag getById(int id);

    List<Tag> getByName(String name);

    void update(Tag tag);

    void delete(int id);
}
