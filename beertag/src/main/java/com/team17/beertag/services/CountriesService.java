package com.team17.beertag.services;

import com.team17.beertag.models.Country;

import java.util.List;

public interface CountriesService {
    Country create(Country country);

    List<Country> getAll();

    Country getById(int id);

    List<Country> getByName(String name);

    void update(Country country);

    void delete(int id);
}
