package com.team17.beertag.services;

import com.team17.beertag.exceptions.EntityNotFoundException;
import com.team17.beertag.models.Beer;
import com.team17.beertag.models.Rating;
import com.team17.beertag.models.UserInfo;
import com.team17.beertag.repositories.BeersRepository;
import com.team17.beertag.repositories.RatingsRepository;
import com.team17.beertag.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class RatingsServiceImpl implements RatingsService {
    private RatingsRepository ratingsRepository;

    @Autowired
    public RatingsServiceImpl(RatingsRepository ratingsRepository) {
        this.ratingsRepository = ratingsRepository;
    }

    @Override
    public Rating create(Rating rating) {
        Beer beer = rating.getBeerId();
        if (beer == null) {
            throw new EntityNotFoundException("Beer");
        }

        UserInfo userInfo = rating.getUserInfoId();
        if (userInfo == null) {
            throw new EntityNotFoundException("User");
        }


        beer.addRating(rating);
        return ratingsRepository.create(rating);
    }

    @Override
    public void update(Rating rating) {
        Beer beer = rating.getBeerId();
        if (beer == null) {
            throw new EntityNotFoundException("Beer");
        }

        UserInfo userInfo = rating.getUserInfoId();
        if (userInfo == null) {
            throw new EntityNotFoundException("User");
        }

        ratingsRepository.update(rating);
    }

    @Override
    public void delete(Rating rating) {
        ratingsRepository.delete(rating);
    }

    @Override
    public double getAverageRatingForBeer(int beerId) {
        return ratingsRepository.getAverageRatingForBeer(beerId);
    }

    @Override
    public List<Rating> getBeersOfUser(int userId) {
        return ratingsRepository.getBeersOfUser(userId);
    }

    @Override
    public List<Beer> getTOPBeersOfUser(int userId) {
        Map<Double, Beer> beerMap = new HashMap<>();

        for (Rating rating : getBeersOfUser(userId)) {
            double avgRating = getAverageRatingForBeer(rating.getBeerId().getId());
            Beer beer = rating.getBeerId();
            beerMap.put(avgRating, beer);
        }

        Map<Double, Beer> map = beerMap.entrySet().stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByKey()))
                .limit(3)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2, LinkedHashMap::new));

        return new ArrayList<>(map.values());
    }


}
