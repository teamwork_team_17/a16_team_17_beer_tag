package com.team17.beertag.services;

import com.team17.beertag.exceptions.DuplicateEntityException;
import com.team17.beertag.models.Tag;
import com.team17.beertag.repositories.BeersRepository;
import com.team17.beertag.repositories.TagsRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static com.team17.beertag.Factory.createTag;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class TagsServiceImplTests {
    @Mock
    TagsRepository repository;

    @InjectMocks
    TagsServiceImpl mockService;

    @Test
    public void createShould_CreateTag_WhenTagNotExists() {
        // Arrange
        Tag tag = createTag();

        // Act
        mockService.create(tag);

        // Assert
        Mockito.verify(repository, times(1))
                .create(tag);
    }

    @Test
    public void createShould_ThrowException_WhenTagAlreadyExists() {
        // Arrange
        Mockito.when(repository.checkTagExists(anyString()))
                .thenReturn(true);
        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockService.create(createTag()));
    }

    @Test
    public void getAllShould_CallRepository() {
        // Arrange
        Tag tag = createTag();
        Mockito.when(repository.getAll())
                .thenReturn(Collections.singletonList(tag));

        // Act
        List<Tag> tags = mockService.getAll();

        // Assert
        Assert.assertSame(1, tags.size());
    }

    @Test
    public void getByIdShould_ReturnTag_WhenTagExists() {
        // Arrange
        Tag tag = createTag();
        Mockito.when(repository.getById(1)).thenReturn(tag);
        // Act, Assert
        Assert.assertSame(tag, mockService.getById(1));
    }

    @Test
    public void getByIdShould_ThrowException_WhenTagNotExists() {
        // Arrange
        Mockito.when(repository.getById(anyInt()))
                .thenReturn(null);
        // Act, Assert
        Assert.assertNull(repository.getById(anyInt()));
    }

    @Test
    public void getByNameShould_CallRepository() {
        // Arrange
        Tag tag = createTag();

        Mockito.when(repository.getByName("testTag"))
                .thenReturn(Collections.singletonList(tag));
        // Act, Assert
        Assert.assertSame(1, mockService.getByName("testTag").size());
    }

    @Test
    public void updateShould_CallRepository() {
        Tag tag = createTag();
        mockService.update(tag);
        Mockito.verify(repository, times(1)).update(tag);
    }

    @Test
    public void deleteShould_CallRepository() {
        mockService.delete(1);
        Mockito.verify(repository, times(1)).delete(1);
    }
}
