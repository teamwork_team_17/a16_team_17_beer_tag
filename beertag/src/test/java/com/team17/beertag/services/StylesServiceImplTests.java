package com.team17.beertag.services;

import com.team17.beertag.exceptions.DuplicateEntityException;
import com.team17.beertag.models.Beer;
import com.team17.beertag.models.Style;
import com.team17.beertag.repositories.BeersRepository;
import com.team17.beertag.repositories.StylesRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static com.team17.beertag.Factory.createBeer;
import static com.team17.beertag.Factory.createStyle;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class StylesServiceImplTests {
    @Mock
    StylesRepository repository;

    @Mock
    BeersRepository beersRepository;

    @InjectMocks
    StylesServiceImpl mockService;

    @Test
    public void createShould_CreateStyle_WhenStyleNotExists() {
        // Arrange
        Style style = createStyle();

        // Act
        mockService.create(style);

        // Assert
        Mockito.verify(repository, times(1))
                .create(style);
    }

    @Test
    public void createShould_ThrowException_WhenStyleAlreadyExists() {
        // Arrange
        Mockito.when(repository.checkStyleExists(anyString()))
                .thenReturn(true);
        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockService.create(createStyle()));
    }

    @Test
    public void getAllShould_CallRepository() {
        // Arrange
        Style style = createStyle();
        Mockito.when(repository.getAll())
                .thenReturn(Collections.singletonList(style));

        // Act
        List<Style> styles = mockService.getAll();

        // Assert
        Assert.assertSame(1, styles.size());
    }

    @Test
    public void getByIdShould_ReturnStyle_WhenStyleExists() {
        // Arrange
        Style style = createStyle();
        Mockito.when(repository.getById(1)).thenReturn(style);
        // Act, Assert
        Assert.assertSame(style, mockService.getById(1));
    }

    @Test
    public void getByIdShould_ThrowException_WhenStyleNotExists() {
        // Arrange
        Mockito.when(repository.getById(anyInt()))
                .thenReturn(null);
        // Act, Assert
        Assert.assertNull(repository.getById(anyInt()));
    }

    @Test
    public void getStyleBeersShould_ReturnBeers() {
        // Arrange
        Style style = createStyle();
        Beer beer = createBeer();
        beer.setStyle(style);

        Mockito.when(beersRepository.getByStyle(1))
                .thenReturn(Collections.singletonList(beer));
        // Act, Assert
        Assert.assertSame(1, mockService.getStyleBeers(1).size());
    }

    @Test
    public void updateShould_CallRepository() {
        Style style = createStyle();
        mockService.update(style);
        Mockito.verify(repository, times(1)).update(style);
    }

    @Test
    public void deleteShould_CallRepository() {
        mockService.delete(1);
        Mockito.verify(repository, times(1)).delete(1);
    }
}