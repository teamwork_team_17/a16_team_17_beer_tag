package com.team17.beertag.services;

import com.team17.beertag.exceptions.DuplicateEntityException;
import com.team17.beertag.models.UserInfo;
import com.team17.beertag.repositories.UsersRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static com.team17.beertag.Factory.createUser;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class UsersServiceImplTests {
    @Mock
    UsersRepository repository;

    @InjectMocks
    UsersServiceImpl mockService;

    @Test
    public void createShould_CallRepository_WhenUserNotExists() {
        // Arrange
        UserInfo expected = createUser();

        // Act
        mockService.create(expected);

        // Assert
        Mockito.verify(repository,
                times(1)).create(expected);
    }

    @Test
    public void createShould_ThrowException_WhenUserAlreadyExists() {
        // Arrange
        UserInfo expected = createUser();
        Mockito.when(repository.checkUserExists(anyString()))
                .thenReturn(true);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockService.create(expected));
    }

    @Test
    public void getAllShould_CallRepository() {
        // Arrange
        UserInfo expected = createUser();
        Mockito.when(repository.getAll())
                .thenReturn(Collections.singletonList(expected));

        // Act
        List<UserInfo> returnedUsers = mockService.getAll();
        // Assert
        Assert.assertSame(1, returnedUsers.size());
    }

    @Test
    public void getByIdShould_ReturnUser_WhenUserExist() {
        // Arrange
        UserInfo expected = createUser();

        Mockito.when(repository.getById(1))
                .thenReturn(expected);

        // Act
        UserInfo returned = mockService.getById(1);

        // Assert
        Assert.assertSame(expected, returned);
    }

    @Test
    public void getByIdShould_ThrowException_WhenUserNotExsists() {
        // Arrange
        Mockito.when(repository.getById(anyInt()))
                .thenReturn(null);

        // Act, Assert
        Assert.assertNull(repository.getById(anyInt()));
    }

    @Test
    public void getByUsernameShould_CallRepository() {
        // Arrange
        UserInfo expected = createUser();
        Mockito.when(repository.getByUsername("testUsername"))
                .thenReturn(Collections.singletonList(expected));

        // Act
        List<UserInfo> returnedUsers = mockService.getByUsername("testUsername");
        // Assert
        Assert.assertSame(1, returnedUsers.size());
    }

    @Test
    public void updateShould_CallRepository() {
        // Arrange
        UserInfo expected = createUser();

        // Act
        mockService.update(expected);

        // Assert
        Mockito.verify(repository, times(1))
                .update(expected);
    }

    @Test
    public void deleteShould_CallRepository() {
        // Act
        mockService.delete(1);

        // Assert
        Mockito.verify(repository, times(1))
                .delete(1);
    }

    @Test
    public void getWishListShould_CallRepository() {
        // Act
        mockService.getWishList(1);
        // Assert
        Mockito.verify(repository, times(1))
                .getWishList(1);
    }

    @Test
    public void getDrankListShould_CallRepository() {
        // Act
        mockService.getDrankList(1);
        // Assert
        Mockito.verify(repository, times(1))
                .getDrankList(1);
    }
}
