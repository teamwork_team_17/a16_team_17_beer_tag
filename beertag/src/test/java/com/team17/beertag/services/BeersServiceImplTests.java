package com.team17.beertag.services;

import com.team17.beertag.exceptions.DuplicateEntityException;
import com.team17.beertag.exceptions.InvalidOperationException;
import com.team17.beertag.models.*;
import com.team17.beertag.repositories.BeersRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.team17.beertag.Factory.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class BeersServiceImplTests {

    @Mock
    BeersRepository repository;

    @InjectMocks
    BeersServiceImpl mockService;



    @Test
    public void createShould_CallRepository_whenBeerNotExists() {
        //Arrange
        Beer expectedBeer = createBeer();

        //Act
        mockService.create(expectedBeer);

        //Assert
        Mockito.verify(repository,
                times(1)).create(expectedBeer);

    }

    @Test
    public void createShould_ThrowException_WhenBeerAlreadyExists() {
        //Arrange
        Mockito.when(repository.checkBeerExists(anyString()))
                .thenReturn(true);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockService.create(createBeer()));
        //mockService.create(createBeer());
    }

    @Test
    public void getAllShould_CallRepository() {
        //Arrange
        Beer beer = createBeer();
        //Beer beer1 = createBeer();
//        List<Beer> beers = new ArrayList<>();
//        beers.add(beer);
        //beers.add(beer1);
        Mockito.when(repository.getAll()).thenReturn(Arrays.asList(beer));

        //Act
        List<Beer> returnedBeers = mockService.getAll();

        //Act, Assert
        Assert.assertSame(1,returnedBeers.size());
    }

    @Test
    public void getFilteredAndSortedShould_CallRepository() {
        //Arrange
        Beer beer = createBeer();
        Country country = createCountry();
        Style style = createStyle();
        beer.setStyle(style);
        beer.setCountry(country);
        List<Beer> beers = new ArrayList<>();
        beers.add(beer);

        Mockito.when(repository.getFilteredAndSorted(country.getId(), style.getId(),"name")).thenReturn(beers);

        //Act
        List<Beer> toReturn = mockService.getFilteredAndSorted(country.getId(), style.getId(),"name");

        //Assert
        Assert.assertSame(1,toReturn.size());
    }



    @Test
    public void getByIdShould_ReturnBeer_WhenBeerExists() {
        //Arrange
        Beer expectedBeer = createBeer();

        Mockito.when(repository.getById(1))
        .thenReturn(expectedBeer);

        //Act
        Beer returnedBeer = mockService.getById(1);

        //Assert
        Assert.assertSame(expectedBeer, returnedBeer);
    }

    @Test
    public void getByIdShould_ThrowException_WhenBeerNotExists() {
        //Arrange

        //Beer beer = createBeer();
        Mockito.when(repository.getById(anyInt()))
                .thenReturn(null);
        //Act, Assert
        Assert.assertNull(repository.getById(anyInt()));
    }

    @Test
    public void getByNameShould_CallRepository() {
        //Arrange
        Beer beer = createBeer();

        Mockito.when(repository.getByName("testBeer")).thenReturn(Arrays.asList(beer));

        List<Beer> returnedBeers = mockService.getByName("testBeer");
        //Act, Assert
        Assert.assertSame(1,returnedBeers.size());
    }

    @Test
    public void getByStyleShould_CallRepository() {
        //Arrange
        Beer beer = createBeer();
        beer.setStyle(createStyle());

        Mockito.when(repository.getByStyle(1)).thenReturn(Arrays.asList(beer));

        List<Beer> returnedBeers = mockService.getByStyle(1);
        //Act, Assert
        Assert.assertSame(1,returnedBeers.size());
    }

    @Test
    public void getByCountryShould_CallRepository() {
        //Arrange
        Beer beer = createBeer();
        beer.setCountry(createCountry());

        Mockito.when(repository.getByCountry(1)).thenReturn(Arrays.asList(beer));

        List<Beer> returnedBeers = mockService.getByCountry(1);
        //Act, Assert
        Assert.assertSame(1,returnedBeers.size());
    }

    @Test
    public void updateShould_CallRepository_When_UserIsCreator(){
        Beer beer = createBeer();
        UserInfo userInfo = createUser();
        beer.setCreatedBy(userInfo);

        mockService.update(beer,userInfo);

        Mockito.verify(repository, times(1)).update(beer);
    }

    @Test
    public void updateShould_CallRepository_When_UserIsAdmin(){
        //Arrange
        Beer beer = createBeer();
        UserInfo userInfo = createUser();
        UserInfo creator = new UserInfo();
        creator.setId(2);
        creator.setUsername("creator");
        beer.setCreatedBy(creator);
        Role role = new Role();
        role.setId(1);
        role.setName("Administrator");
        List<Role> roles = new ArrayList<>();
        roles.add(role);
        userInfo.setRoles(roles);

        //Act
        mockService.update(beer,userInfo);

        //Assert
        Mockito.verify(repository, times(1)).update(beer);
    }

    @Test
    public void updateShould_ThrowException_When_UserIsNotAdminOrCreator() {
        //Arrange
        Beer beer = createBeer();
        UserInfo userInfo = createUser();
        UserInfo creator = new UserInfo();
        creator.setId(2);
        creator.setUsername("creator");
        beer.setCreatedBy(creator);
        Role role = new Role();
        role.setId(1);
        role.setName("User");
        List<Role> roles = new ArrayList<>();
        roles.add(role);
        userInfo.setRoles(roles);

        //Act, Assert
        Assertions.assertThrows(InvalidOperationException.class,
                () -> mockService.update(beer, userInfo));
    }

    @Test
    public void deleteShould_CallRepository_When_UserIsCreator(){
        Beer beer = createBeer();
        UserInfo userInfo = createUser();
        beer.setCreatedBy(userInfo);
        Mockito.when(repository.getById(1)).thenReturn(beer);

        mockService.delete(1,userInfo);

        Mockito.verify(repository, times(1)).delete(1);
    }

    @Test
    public void deleteShould_CallRepository_When_UserIsAdmin(){
        //Arrange
        Beer beer = createBeer();
        UserInfo userInfo = createUser();
        UserInfo creator = new UserInfo();
        creator.setId(2);
        creator.setUsername("creator");
        beer.setCreatedBy(creator);
        Role role = new Role();
        role.setId(1);
        role.setName("Administrator");
        List<Role> roles = new ArrayList<>();
        roles.add(role);
        userInfo.setRoles(roles);

        Mockito.when(repository.getById(1)).thenReturn(beer);
        //Act
        mockService.delete(1,userInfo);

        //Assert
        Mockito.verify(repository, times(1)).delete(1);
    }

    @Test
    public void deleteShould_ThrowException_When_UserIsNotAdminOrCreator() {
        //Arrange
        Beer beer = createBeer();
        UserInfo userInfo = createUser();
        UserInfo creator = new UserInfo();
        creator.setId(2);
        creator.setUsername("creator");
        beer.setCreatedBy(creator);
        Role role = new Role();
        role.setId(1);
        role.setName("User");
        List<Role> roles = new ArrayList<>();
        roles.add(role);
        userInfo.setRoles(roles);
        Mockito.when(repository.getById(1)).thenReturn(beer);

        //Act, Assert
        Assertions.assertThrows(InvalidOperationException.class,
                () -> mockService.delete(1, userInfo));
    }

    @Test
    public void addBeerToWishListShould_CallRepository() {
        // Act
        mockService.addBeerToWishList(1, 1);

        // Assert
        Mockito.verify(repository, times(1))
                .addBeerToWishList(1, 1);
    }

    @Test
    public void addBeerToDrankListShould_CallRepository() {
        // Act
        mockService.addBeerToDrankList(1, 1);

        // Assert
        Mockito.verify(repository, times(1))
                .addBeerToDrankList(1, 1);
    }

    @Test
    public void deleteBeerFromWishListShould_CallRepository() {
        // Act
        mockService.deleteBeerFromWishList(1, 1);

        // Assert
        Mockito.verify(repository, times(1))
                .deleteBeerFromWishList(1, 1);
    }
}
