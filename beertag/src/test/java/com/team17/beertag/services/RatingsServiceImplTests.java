package com.team17.beertag.services;

import com.team17.beertag.exceptions.EntityNotFoundException;
import com.team17.beertag.models.Beer;
import com.team17.beertag.models.Rating;
import com.team17.beertag.repositories.RatingsRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static com.team17.beertag.Factory.*;

@RunWith(MockitoJUnitRunner.class)
public class RatingsServiceImplTests {
    @Mock
    RatingsRepository repository;

    @InjectMocks
    RatingsServiceImpl mockService;

    @Test
    public void createShould_ThrowException_WhenBeerNotFound() {
        //Arrange
        Rating rating = new Rating();
        rating.setUserInfoId(createUser());
        rating.setRating(2.50);
        rating.setBeerId(null);

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockService.create(rating));
    }

    @Test
    public void createShould_ThrowException_WhenUserNotFound() {
        //Arrange
        Rating rating = new Rating();
        rating.setUserInfoId(null);
        rating.setRating(2.50);
        rating.setBeerId(createBeer());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockService.create(rating));
    }

    @Test
    public void createShould_CallRepository_WhenUserAndBeerExists() {
        //Arrange
        Rating rating = createRating();

        //Act
        mockService.create(rating);

        //Assert
        Mockito.verify(repository, Mockito.times(1)).create(rating);
    }

    @Test
    public void updateShould_ThrowException_WhenBeerNotFound() {
        //Arrange
        Rating rating = new Rating();
        rating.setUserInfoId(createUser());
        rating.setRating(2.50);
        rating.setBeerId(null);

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockService.update(rating));
    }

    @Test
    public void updateShould_ThrowException_WhenUserNotFound() {
        //Arrange
        Rating rating = new Rating();
        rating.setUserInfoId(null);
        rating.setRating(2.50);
        rating.setBeerId(createBeer());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockService.update(rating));
    }

    @Test
    public void updateShould_CallRepository_WhenUserAndBeerExists() {
        //Arrange
        Rating rating = createRating();

        //Act
        mockService.update(rating);

        //Assert
        Mockito.verify(repository, Mockito.times(1)).update(rating);
    }

    @Test
    public void deleteShould_CallRepository() {
        Rating rating = createRating();
        mockService.delete(rating);
        Mockito.verify(repository, Mockito.times(1)).delete(rating);
    }

    @Test
    public void getAverageRatingShould_CallRepository() {
        Beer beer = createBeer();
        mockService.getAverageRatingForBeer(beer.getId());
        Mockito.verify(repository, Mockito.times(1)).getAverageRatingForBeer(beer.getId());
    }

    @Test
    public void getBeersOfUserShould_CallRepository() {
        //Arrange
        Rating rating = createRating();
        Mockito.when(repository.getBeersOfUser(rating.getUserInfoId().getId()))
                .thenReturn(Arrays.asList(rating));

        //Act, Assert
        Assert.assertSame(1, mockService.getBeersOfUser(rating.getUserInfoId().getId()).size());

    }

    @Test
    public void getTOPBeersOfUser() {
        // Arrange

        // Act

        // Assert
    }
}
