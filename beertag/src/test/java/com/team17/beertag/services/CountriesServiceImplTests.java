package com.team17.beertag.services;

import com.team17.beertag.exceptions.DuplicateEntityException;
import com.team17.beertag.models.Country;
import com.team17.beertag.repositories.CountriesRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import static com.team17.beertag.Factory.createCountry;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)

public class CountriesServiceImplTests {

    @Mock
    CountriesRepository repository;

    @InjectMocks
    CountriesServiceImpl mockService;

    @Test
    public void createShould_CreateCountry_WhenCountryNotExists() {
        //Arrange
        Country country = createCountry();

        //Act
        mockService.create(country);

        //Assert
        Mockito.verify(repository,
                times(1)).create(country);
    }

    @Test
    public void createShould_ThrowException_WhenCountryAlreadyExists() {
        //Arrange
        Mockito.when(repository.checkCountryExists(anyString()))
                .thenReturn(true);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockService.create(createCountry()));

    }

    @Test
    public void getAllShould_CallRepository() {
        //Arrange
        Country country = createCountry();
        Mockito.when(repository.getAll()).thenReturn(Arrays.asList(country));

        //Act, Assert
        Assert.assertSame(1,mockService.getAll().size());
    }

    @Test
    public void getByIdShould_ReturnCountry_WhenCountryExists() {
        //Arrange
        Country country = createCountry();

        Mockito.when(repository.getById(1))
                .thenReturn(country);

        //Act, Assert
        Assert.assertSame(country, mockService.getById(1));
    }

    @Test
    public void getByIdShould_ThrowException_WhenCountryNotExists() {
        //Arrange

        Mockito.when(repository.getById(anyInt()))
                .thenReturn(null);
        //Act, Assert
        Assert.assertNull(repository.getById(anyInt()));
    }

    @Test
    public void getByNameShould_CallRepository() {
        //Arrange
        Country country = createCountry();

        Mockito.when(repository.getByName("testCountry")).thenReturn(Arrays.asList(country));

        //Act, Assert
        Assert.assertSame(1,mockService.getByName("testCountry").size());
    }

    @Test
    public void updateShould_CallRepository() {
        Country country = createCountry();
        mockService.update(country);
        Mockito.verify(repository, Mockito.times(1)).update(country);
    }

    @Test
    public void deleteShould_CallRepository() {
        mockService.delete(1);
        Mockito.verify(repository, Mockito.times(1)).delete(1);
    }
}
