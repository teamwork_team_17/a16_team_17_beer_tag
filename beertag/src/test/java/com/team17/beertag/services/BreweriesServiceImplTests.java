package com.team17.beertag.services;

import com.team17.beertag.exceptions.DuplicateEntityException;
import com.team17.beertag.models.Beer;
import com.team17.beertag.models.Brewery;
import com.team17.beertag.repositories.BeersRepository;
import com.team17.beertag.repositories.BreweriesRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static com.team17.beertag.Factory.createBeer;
import static com.team17.beertag.Factory.createBrewery;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class BreweriesServiceImplTests {

    @Mock
    BreweriesRepository repository;

    @Mock
    BeersRepository beersRepository;

    @InjectMocks
    BreweriesServiceImpl mockService;

    @Test
    public void createShould_CreateBrewery_WhenBreweryNotExists() {
        //Arrange
        Brewery brewery = createBrewery();

        //Act
        mockService.create(brewery);

        //Assert
        Mockito.verify(repository,
                times(1)).create(brewery);
    }

    @Test
    public void createShould_ThrowException_WhenBreweryAlreadyExists() {
        //Arrange
        Mockito.when(repository.checkBreweryExists(anyString()))
                .thenReturn(true);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockService.create(createBrewery()));

    }

    @Test
    public void getAllShould_CallRepository() {
        //Arrange
        Brewery brewery = createBrewery();
        Mockito.when(repository.getAll()).thenReturn(Arrays.asList(brewery));

        //Act
        List<Brewery> breweries = mockService.getAll();

        //Assert
        Assert.assertSame(1, breweries.size());
    }

    @Test
    public void getByIdShould_ReturnBrewery_WhenBreweryExists() {
        //Arrange
        Brewery brewery = createBrewery();

        Mockito.when(repository.getById(1))
                .thenReturn(brewery);

        //Act, Assert
        Assert.assertSame(brewery, mockService.getById(1));
    }

    @Test
    public void getByIdShould_ThrowException_WhenBreweryNotExists() {
        //Arrange

        Mockito.when(repository.getById(anyInt()))
                .thenReturn(null);
        //Act, Assert
        Assert.assertNull(repository.getById(anyInt()));
    }

    @Test
    public void getBeersByBreweryShould_ReturnBeers() {
        //Arrange
        Beer beer = createBeer();
        Brewery brewery = createBrewery();
        beer.setBrewery(brewery);

        Mockito.when(beersRepository.getByBrewery(1))
                .thenReturn(Arrays.asList(beer));

        //Act, Assert
        Assert.assertSame(1, mockService.getBreweryBeers(1).size());
    }

    @Test
    public void getByNameShould_CallRepository() {
        //Arrange
        Brewery brewery = createBrewery();

        Mockito.when(repository.getByName("testBrewery")).thenReturn(Arrays.asList(brewery));

        //Act, Assert
        Assert.assertSame(1, mockService.getByName("testBrewery").size());
    }

    @Test
    public void updateShould_CallRepository() {
        Brewery brewery = createBrewery();
        mockService.update(brewery);
        Mockito.verify(repository, Mockito.times(1)).update(brewery);
    }

    @Test
    public void deleteShould_CallRepository() {
        mockService.delete(1);
        Mockito.verify(repository, Mockito.times(1)).delete(1);
    }
}
