package com.team17.beertag;

import com.team17.beertag.models.*;

public class Factory {
    public static Beer createBeer() {
        Beer beer = new Beer();
        beer.setId(1);
        beer.setName("testBeer");
        beer.setAbv(1.1);
        return beer;
    }

    public static UserInfo createUser() {
        UserInfo userInfo = new UserInfo();
        userInfo.setUsername("testUser");
        userInfo.setId(1);
        userInfo.setUsername("testUsername");
        userInfo.setPassword("testPassword");
        userInfo.setEmail("testEmail@test.com");
        userInfo.setFirstName("testFirstName");
        userInfo.setLastName("testLastName");
        return userInfo;
    }

    public static Country createCountry() {
        Country country = new Country();
        country.setId(1);
        country.setName("testCountry");
        return country;
    }

    public static Style createStyle() {
        Style style = new Style();
        style.setId(1);
        style.setName("testStyle");
        return style;
    }

    public static Tag createTag() {
        Tag tag = new Tag();
        tag.setId(1);
        tag.setName("testTag");
        return tag;
    }

    public static Rating createRating() {
        Rating rating = new Rating();
        rating.setRating(2.50);
        rating.setUserInfoId(createUser());
        rating.setBeerId(createBeer());
        return rating;
    }

    public static Beer createBeerWithCountryAndStyle() {
        Beer beer = new Beer();
        beer.setCountry(createCountry());
        beer.setStyle(createStyle());
        return beer;
    }

    public static Brewery createBrewery() {
        Brewery brewery = new Brewery();
        brewery.setId(1);
        brewery.setName("testBrewery");
        return brewery;
    }
}
